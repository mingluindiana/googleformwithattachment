/* Renders the form */
function doGet(e) {
  var output = HtmlService.createHtmlOutputFromFile('form.html');
  output.setTitle('USIC Ticket Form');
  return output;
}
 
/* uploading function */
function uploadFiles(form) {
  
  try {
    
    /* Name of the Drive folder where the files should be saved */
    var dropbox = "Tickets Attachment";
    var folder, folders = DriveApp.getFoldersByName(dropbox);
    
    /* Find the folder, create if the folder does not exist */
    if (folders.hasNext()) {
      folder = folders.next();
    } else {
      folder = DriveApp.createFolder(dropbox);
    }
    
    /* Store Data into Resp Spreadsheet */
      var resp = SpreadsheetApp.openById("1DiOMZpKCBuSlvknTdE1vNBXWc72txNAhMAwU5MoX5k0");
      var first = resp.getSheetByName("Sheet1");
      first.activate();
    
    /* Get the file uploaded though the form as a blob */

    var blob1 = form.myFile1; 
    var blob2 = form.myFile2;
    var file1URL = "";
    var file2URL = "";
    
    if (form.file1Flag == "1"){
          var file1 = folder.createFile(blob1); 
          file1.setDescription("Uploaded by " + form.myName);
          file1URL = file1.getUrl();
    }
    
    if (form.file2Flag == "1"){
          var file2 = folder.createFile(blob2); 
          file2.setDescription("Uploaded by " + form.myName);
          file2URL = file2.getUrl();
    }
             
    /* Append Row */  
    first.appendRow([form.myName, form.myCompany, form.myMaxNo,form.myDesc,form.myCity,form.myState,form.myCounty,form.myTown,form.myDate,form.myFootage,file1URL,file2URL]);
        
    /* Message */
    var email = "usic.designsubmittal@gmail.com"
    var subject = form.myName + " submitted a ticket"
    var message = "Check out response @ https://docs.google.com/spreadsheets/d/1DiOMZpKCBuSlvknTdE1vNBXWc72txNAhMAwU5MoX5k0/edit#gid=0"
    
    MailApp.sendEmail(email, subject, message); 
      
    return "Successfully Submitted";
    
  } catch (error) {
    
    /* If there's an error, show the error message */
    return error.toString();
  }
  
}